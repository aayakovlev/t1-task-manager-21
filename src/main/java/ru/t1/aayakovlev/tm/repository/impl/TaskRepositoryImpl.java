package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepositoryImpl extends AbstractUserOwnedRepository<Task> implements TaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        return save(userId, task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return save(userId, task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models.stream()
                .filter((m) -> m.getProjectId() != null)
                .filter((m) -> projectId.equals(m.getProjectId()))
                .filter((m) -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
