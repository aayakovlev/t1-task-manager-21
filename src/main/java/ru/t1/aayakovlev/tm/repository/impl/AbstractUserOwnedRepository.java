package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractBaseRepository<M>
        implements UserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int count(final String userId) {
        return findAll(userId).size();
    }

    @Override
    public boolean existsById(final String userId, final String id) throws AbstractFieldException {
        return findById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter((m) -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findById(final String userId, final String id) throws AbstractFieldException {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter((m) -> userId.equals(m.getUserId()))
                .filter((m) -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M remove(final String userId, final M model) throws AbstractFieldException {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) throws AbstractFieldException {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M save(final String userId, final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return save(model);
    }

}
