package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.AbstractEntityException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractModel;

import java.util.List;

public interface BaseRepository<M extends AbstractModel> {

    void clear();

    List<M> findAll();

    M findById(final String id) throws AbstractFieldException;

    M remove(final M model) throws AbstractEntityException;

    M removeById(final String id) throws AbstractException;

    void removeAll(final List<M> models);

    M save(final M model);

}
