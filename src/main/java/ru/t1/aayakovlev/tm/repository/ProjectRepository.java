package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectRepository extends UserOwnedRepository<Project> {

    Project create(final String userId, final String name);

    Project create(final String userId, final String name, final String description);

}
