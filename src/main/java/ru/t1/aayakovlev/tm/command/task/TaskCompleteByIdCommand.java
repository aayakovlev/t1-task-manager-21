package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Task complete by id.";

    public static final String NAME = "task-complete-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final String userId = getUserId();
        getTaskService().changeStatusById(userId, id, Status.COMPLETED);
    }

}
