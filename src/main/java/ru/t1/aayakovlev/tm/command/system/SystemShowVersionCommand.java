package ru.t1.aayakovlev.tm.command.system;

public final class SystemShowVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "Show application version.";

    public static final String NAME = "version";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.21.0");
    }

}
