package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected TaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(final List<Task> tasks) {
        final AtomicInteger index = new AtomicInteger(1);
        tasks.stream()
                .filter(Objects::nonNull)
                .forEachOrdered((t) -> System.out.println(index.getAndIncrement() + ". " + t));
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Project id: " + task.getProjectId());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

}
