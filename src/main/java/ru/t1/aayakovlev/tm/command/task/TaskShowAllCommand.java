package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskShowAllCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show all tasks.";

    public static final String NAME = "task-show-all";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW ALL TASKS]");
        System.out.println("Sorts to enter:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}
