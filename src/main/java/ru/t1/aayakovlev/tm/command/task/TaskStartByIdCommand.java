package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Start task by id.";

    public static final String NAME = "task-start-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final String userId = getUserId();
        getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
    }

}
