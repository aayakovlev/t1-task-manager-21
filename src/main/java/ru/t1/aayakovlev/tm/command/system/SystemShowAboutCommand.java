package ru.t1.aayakovlev.tm.command.system;

public final class SystemShowAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Show developer info.";

    public static final String NAME = "about";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("email: aayakovlev@t1-consulting.ru");
        System.out.println("name: Yakovlev Anton.");
    }

}
