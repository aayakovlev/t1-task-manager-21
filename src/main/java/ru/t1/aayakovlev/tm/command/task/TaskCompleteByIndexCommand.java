package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Task complete by index.";

    public static final String NAME = "task-complete-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
    }

}
