package ru.t1.aayakovlev.tm.model;

import ru.t1.aayakovlev.tm.enumerated.Status;

public final class Project extends AbstractUserOwnedModel implements WBS {

    public Project() {
    }

    public Project(final String name, final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public Project(final String name, final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

}
