package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;


public interface BaseService<M extends AbstractModel> extends BaseRepository<M> {

}
