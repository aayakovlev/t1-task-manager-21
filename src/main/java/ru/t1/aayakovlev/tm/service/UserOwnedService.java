package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;

import java.util.List;

public interface UserOwnedService<M extends AbstractUserOwnedModel> extends BaseService<M>, UserOwnedRepository<M> {

    M changeStatusById(final String userId, final String id, final Status status) throws AbstractException;

    M changeStatusByIndex(final String userId, final Integer index, final Status status) throws AbstractException;

    List<M> findAll(final String userId, final Sort sort) throws UserIdEmptyException;

    M updateById(final String userId, final String id, final String name, final String description) throws AbstractException;

    M updateByIndex(final String userId, final Integer index, final String name, final String description) throws AbstractException;

}
