package ru.t1.aayakovlev.tm.service;

public interface ServiceLocator {

    AuthService getAuthService();

    CommandService getCommandService();

    LoggerService getLoggerService();

    ProjectService getProjectService();

    ProjectTaskService getProjectTaskService();

    TaskService getTaskService();

    UserService getUserService();

}
